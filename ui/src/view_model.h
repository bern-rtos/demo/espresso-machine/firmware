#ifndef VIEW_MODEL_H
#define VIEW_MODEL_H

typedef struct {
    float boiler_outside;
    float boiler_inside;
    float group_head_outside;
    float group_head_inside;
} MODEL_temperature_t;

typedef struct {
    float pressure_water;
    float volume_water;
    MODEL_temperature_t temperature;
    float time;
} MODEL_view_model_t;

#endif //VIEW_MODEL_H
