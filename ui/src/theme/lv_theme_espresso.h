#ifndef LV_THEME_ESPRESSO_H
#define LV_THEME_ESPRESSO_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lvgl/src/core/lv_obj.h"

/*********************
 *      DEFINES
 *********************/

#ifdef DISP_RGB_MAP_BGR
# define DISP_COLOR_MAP(hex) (((hex & 0xFF0000) >> 16) | ((hex & 0x0000FF) << 16) | (hex & 0x00FF00))
#else
# define DISP_COLOR_MAP(hex) (hex)
#endif

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/

/**
 * Initialize the theme
 * @param color_primary the primary color of the theme
 * @param color_secondary the secondary color for the theme
 * @param font pointer to a font to use.
 * @return a pointer to reference this theme later
 */
lv_theme_t * lv_theme_espresso_init(lv_disp_t * disp, lv_color_t color_primary, lv_color_t color_secondary, bool dark,
                                   const lv_font_t * font);

/**
 * Get default theme
 * @return a pointer to default theme, or NULL if this is not initialized
 */
lv_theme_t * lv_theme_espresso_get(void);

/**
 * Check if default theme is initialized
 * @return true if default theme is initialized, false otherwise
 */
bool lv_theme_espresso_is_inited(void);

/**********************
 *      MACROS
 **********************/


#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif /*LV_THEME_ESPRESSO_H*/
