#define _DEFAULT_SOURCE /* needed for usleep() */
#include <unistd.h>

#include "ui.h"

int main(int argc, char **argv) {
    (void) argc; /*Unused*/
    (void) argv; /*Unused*/

    static MODEL_view_model_t view_model = {
            .time = 12.0f,
            .pressure_water = 9.2f,
            .volume_water = 23.0f,
            .temperature = {
                    .boiler_outside = 95.3f,
                    .boiler_inside = 102.5f,
                    .group_head_outside = 5.0f,
                    .group_head_inside = 87.1f,
            },
    };

    UI_init();

    while (1) {
        UI_update(&view_model);
        UI_tick();
        usleep(5 * 1000);
    }
}