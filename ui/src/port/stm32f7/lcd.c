#include "lcd.h"

#include <stdlib.h>

#include "lv_conf.h"
#include "lvgl/lvgl.h"
#include "theme/lv_theme_espresso.h"
#include "theme/material_palette.h"

static lv_disp_drv_t disp_drv;
static const lv_color_t * buf_to_flush;

static volatile lv_disp_t *disp = NULL;

#if LV_COLOR_DEPTH == 16
typedef uint16_t uintpixel_t;
#elif LV_COLOR_DEPTH == 24 || LV_COLOR_DEPTH == 32
typedef uint32_t uintpixel_t;
#endif


static volatile uintpixel_t* my_fb = (volatile uintpixel_t*) (0xC0000000);
//static uintpixel_t my_fb[LCD_RES_H*LCD_RES_V] __attribute__((section(".gui")));

static void ex_disp_flush(lv_disp_drv_t *drv, const lv_area_t *area, lv_color_t * color_p);
static void input_dummy(lv_indev_drv_t *drv, lv_indev_data_t *data);

#define BUFFER_B_LINES 8

void LCD_init(void) {
    static lv_disp_draw_buf_t disp_buf_1;
    static lv_color_t buf1_1[LCD_RES_H * BUFFER_B_LINES];
    static lv_color_t buf1_2[LCD_RES_H * BUFFER_B_LINES];
    lv_disp_draw_buf_init(&disp_buf_1, buf1_1, buf1_2, LCD_RES_H * BUFFER_B_LINES);   /*Initialize the display buffer*/


    /*-----------------------------------
    * Register the display in LittlevGL
    *----------------------------------*/

    lv_disp_drv_init(&disp_drv);                    /*Basic initialization*/

    disp_drv.hor_res = LCD_RES_H;
    disp_drv.ver_res = LCD_RES_V;
    disp_drv.flush_cb = ex_disp_flush;
    disp_drv.draw_buf = &disp_buf_1;
    disp_drv.sw_rotate = 1;
    disp_drv.rotated = LV_DISP_ROT_90;

    disp = lv_disp_drv_register(&disp_drv);

    lv_theme_t *th = lv_theme_espresso_init(disp, MATERIAL_COLOR_PRIMARY, MATERIAL_COLOR_SECONDARY,
                                            true, LV_FONT_DEFAULT);
    lv_disp_set_theme(disp, th);
}

/* Flush the content of the internal buffer the specific area on the display
 * You can use DMA or any hardware acceleration to do this operation in the background but
 * 'lv_flush_ready()' has to be called when finished
 * This function is required only when LV_VDB_SIZE != 0 in lv_conf.h*/
static void ex_disp_flush(lv_disp_drv_t *drv, const lv_area_t *area, lv_color_t * color_p) {
    int32_t x1 = area->x1;
    int32_t x2 = area->x2;
    int32_t y1 = area->y1;
    int32_t y2 = area->y2;
    /*Return if the area is out the screen*/

    if (x2 < 0) return;
    if (y2 < 0) return;
    if (x1 > LCD_RES_H - 1) return;
    if (y1 > LCD_RES_V - 1) return;

    /*Truncate the area to the screen*/
    int32_t act_x1 = x1 < 0 ? 0 : x1;
    int32_t act_y1 = y1 < 0 ? 0 : y1;
    int32_t act_x2 = x2 > LCD_RES_H - 1 ? LCD_RES_H - 1 : x2;
    int32_t act_y2 = y2 > LCD_RES_V - 1 ? LCD_RES_V - 1 : y2;

    buf_to_flush = color_p;
    int32_t offset_x = act_x1;
    uint32_t size_x = act_x2 - act_x1 + 1;
    int32_t offset_y = act_y1;
    uint32_t size_y = act_y2 - act_y1 + 1;

    // todo: flush using DMA
    for (size_t y = 0; y < size_y; ++y) {
        for (size_t x = 0; x < size_x; ++x) {
            my_fb[(y + offset_y) * LCD_RES_H + x + offset_x] = ((uintpixel_t *)buf_to_flush)[x + y * size_x];
        }
    }

    lv_disp_flush_ready(&disp_drv);
}