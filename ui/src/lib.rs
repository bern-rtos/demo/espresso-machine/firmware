#![no_std]

mod wrapper;
use wrapper::*;

pub use wrapper::MODEL_view_model_t as ViewModel;
pub use wrapper::MODEL_temperature_t as Temperature;

pub struct UI {}

impl UI {
    pub const fn new() -> Self {
        UI {

        }
    }

    pub fn init(&self) {
        unsafe {
            UI_init();
        }
    }

    pub fn update(&self, view_model: &ViewModel) {
        unsafe {
            UI_update(view_model as *const _);
        }
    }

    pub fn tick(&self) {
        unsafe {
            UI_tick();
        }
    }
}
