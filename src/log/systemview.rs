use systemview_target::SystemView;
rtos_trace::global_trace!{SystemView}

struct Application;
rtos_trace::global_application_callbacks!{Application}

impl rtos_trace::RtosTraceApplicationCallbacks for Application {
    fn system_description() {
        systemview_target::send_system_desc_app_name!("Espresso Machine");
        systemview_target::send_system_desc_device!("STM32F769NI");
        systemview_target::send_system_desc_core!("Cortex-M7");
        systemview_target::send_system_desc_os!("Bern RTOS");
        systemview_target::send_system_desc_interrupt!(15, "SysTick");
        systemview_target::send_system_desc_interrupt!(11, "SysCall");
        systemview_target::send_system_desc_interrupt!(77, "ETH_DMA");
    }

    fn sysclock() -> u32 {
        board::SYSCLK.to_Hz()
    }
}

#[no_mangle]
pub unsafe extern "C" fn systemview_get_timestamp() -> u32 {
    bern_kernel::syscall::core_debug_time()
}