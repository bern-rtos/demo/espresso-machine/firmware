use bern_kernel::exec::process::{Process, ProcessError};
use bern_kernel::exec::runnable::Priority;
use bern_kernel::exec::thread::Thread;
use bern_kernel::mem::queue::spsc_const::ConstQueue;
use bern_kernel::sleep;
use bern_kernel::stack::Stack;
use bern_kernel::sync::ipc::channel::IpcReceiver;
use board::display::Display;
use board::hal::ltdc::{Layer, PixelFormat};
use crate::process::machine_control::MeasData;

static UI_PROC: &Process = bern_kernel::new_process!(ui, 16384);

pub struct Peripherals {
    pub display: Display,
}

pub struct Ipc<const N: usize> {
    pub meas_receiver: IpcReceiver<ConstQueue<MeasData, { N }>>,
    pub suspend_receiver: IpcReceiver<ConstQueue<bool, { N }>>,
}

pub static UI_MEAS_QUEUE: ConstQueue<MeasData, 2> = ConstQueue::new();
pub static UI_SUSPEND_QUEUE: ConstQueue<bool, 2> = ConstQueue::new();

static UI: ui::UI = ui::UI::new();

#[derive(PartialEq)]
enum UiState {
    Suspended,
    Active,
}

pub fn init<const N: usize>(peripherals: Peripherals, ipc: Ipc<{ N }>) -> Result<(), ProcessError> {
    let mut display = peripherals.display;
    let meas_receiver = ipc.meas_receiver;
    let suspend_receiver = ipc.suspend_receiver;

    UI_PROC.init(move |c| {
        display.controller.config_layer(
            Layer::L1, unsafe { &mut (*(0xC0000000 as *mut [u32; 384000])) },
            PixelFormat::ARGB8888
        );

        display.controller.enable_layer(Layer::L1);
        display.controller.reload();

        Thread::new(c)
            .priority(Priority::new(5))
            .stack(Stack::try_new_in(c, 8192).unwrap())
            .name("GUI\0")
            .spawn(move || {
                let mut view_model = ui::ViewModel {
                    pressure_water: 0.0_f32,
                    volume_water: 0.0_f32,
                    temperature: ui::Temperature {
                        boiler_inside: 0.0_f32,
                        boiler_outside: 0.0_f32,
                        group_head_inside: 0.0_f32,
                        group_head_outside: 0.0_f32,
                    },
                    time: 0.0_f32,
                };

                UI.init();
                // draw for the first time
                UI.update(&view_model);
                UI.tick();

                //display.enable();

                let mut state = UiState::Suspended;

                loop {
                    match state {
                        UiState::Suspended => {
                            suspend_receiver
                                .recv()
                                .and_then(|s| {
                                    if !s {
                                        state = UiState::Active;
                                        display.enable();
                                    }
                                    Ok(s)
                                })
                                .ok();

                        }
                        UiState::Active => {
                            meas_receiver
                                .recv()
                                .and_then(|m| {
                                    view_model.temperature.boiler_inside = m.temperature.boiler_inside;
                                    view_model.temperature.boiler_outside = m.temperature.boiler_outside;
                                    view_model.temperature.group_head_outside = m.temperature.group_head_outside;
                                    view_model.pressure_water = m.water_pressure;
                                    view_model.volume_water = m.water_volume;
                                    view_model.time = m.shot_time;
                                    Ok(m)
                                })
                                .ok();
                            UI.update(&view_model);
                            UI.tick();

                            suspend_receiver
                                .recv()
                                .and_then(|s| {
                                    if s {
                                        state = UiState::Suspended;
                                        display.disable();
                                    }
                                    Ok(s)
                                })
                                .ok();
                        }
                    }

                    sleep(5);
                }
            });
    })
}