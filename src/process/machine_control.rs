mod state_space;

use alloc::sync::Arc;
use core::fmt::Debug;
use serde::Serialize;

use bern_kernel::exec::process::{Process, ProcessError};
use bern_kernel::exec::runnable::Priority;
use bern_kernel::exec::thread::Thread;
use bern_kernel::{sleep, syscall};
use bern_kernel::exec::interrupt::{InterruptHandler, InterruptStack};
use bern_kernel::stack::Stack;
use bern_kernel::sync::ipc::channel::IpcSender;
use bern_kernel::sync::{Mutex, Semaphore};
use ad7124::Ad7124;
use ad7124::preset::CALLENDAR_VAN_DUSEN_IEC60751;
use ad7124::preset::rtd_3wire::*;
use ad7124::register::*;
use embedded_hal::blocking::spi::Transfer;
use bern_kernel::log::{info, warn};
use board::{actuators, flow_sensor};
use board::analog_sensors;
use board::buttons;
use board::state;

use board::traits::{Actuators, Buttons, Led};

static MACHINE_CONTROL: &Process = bern_kernel::new_process!(machine_control, 32768);

pub struct Peripherals<Spi> {
    pub actuators: actuators::Actuators,
    pub buttons: buttons::Buttons,
    pub state: state::State,
    pub spi: Spi,
    pub analog_sensors: analog_sensors::AnalogSensors,
    pub flow_sensor: flow_sensor::FlowSensor,
}

pub struct Ipc {
    pub log_sender: IpcSender<MeasData>,
    pub log_ctrl_sender: IpcSender<ControllerData>,
    pub ui_sender: IpcSender<MeasData>,
    pub ui_suspend_sender: IpcSender<bool>,
}

struct ControlRequest {
    pub boiler: bool,
}

#[derive(Copy, Clone, Serialize, Default)]
pub struct MeasTemperature {
    pub boiler_inside: f32,
    pub boiler_outside: f32,
    pub group_head_outside: f32,
    pub group_head_inside: f32,
}

#[derive(Copy, Clone, Serialize)]
pub struct ControllerData {
    pub x: f32,
    pub e: f32,
    pub y: f32,
    pub internal_1: f32,
    pub internal_2: f32,
    pub internal_3: f32,
    pub internal_4: f32,
}

#[derive(Copy, Clone, Serialize, Default)]
pub struct MeasData {
    pub t: u32,
    pub temperature: MeasTemperature,
    pub water_pressure: f32,
    pub water_volume: f32,
    pub shot_time: f32,
}

#[derive(PartialEq)]
enum EspressoState {
    Off,
    Idle,
    PullShot,
    WaitForRelease,
}

const CIRCUIT: Circuit<3> = Circuit {
    rtd_ios: [
        RTD3WIO {
            sense_p: Input::AIN1,
            sense_n: Input::AIN2,
            source_p: 0,
            source_n: 3,
        },
        RTD3WIO {
            sense_p: Input::AIN5,
            sense_n: Input::AIN6,
            source_p: 4,
            source_n: 7,
        },
        RTD3WIO {
            sense_p: Input::AIN9,
            sense_n: Input::AIN10,
            source_p: 8,
            source_n: 11,
        },
    ],
    ref_resistor: 5110,
};

macro_rules! block_or_sleep {
    ($e:expr) => {
        loop {
            #[allow(unreachable_patterns)]
            match $e {
                Err(nb::Error::Other(e)) =>
                {
                    #[allow(unreachable_code)]
                    break Err(e)
                }
                Err(nb::Error::WouldBlock) => {}
                Ok(x) => break Ok(x),
            }
            sleep(1);
        }
    };
}

pub fn init<Spi, E>(peripherals: Peripherals<Spi>, ipc: Ipc) -> Result<(), ProcessError>
    where
        Spi: Transfer<u8, Error=E> + 'static,
        E: Debug,
{
    let state = peripherals.state;
    let buttons = peripherals.buttons;
    let mut actuators = peripherals.actuators;
    let spi = peripherals.spi;
    let mut analog_sensors = peripherals.analog_sensors;
    let flow_sensor = peripherals.flow_sensor;

    let log_sender = ipc.log_sender;
    let log_ctrl_sender = ipc.log_ctrl_sender;
    let meas_sender = ipc.ui_sender;
    let ui_suspend_sender = ipc.ui_suspend_sender;

    MACHINE_CONTROL.init(move |c| {
        let meas = Arc::new(Mutex::new(MeasData::default()));
        let ctrl_req = Arc::new(Mutex::new(ControlRequest { boiler: false }));
        let flow = Arc::new(flow_sensor);
        let shot_time = Arc::new(Mutex::new(0.0_f32));
        let suspend_control = Arc::new(Semaphore::new(0));

        let mut state_ok = state.ok;
        Thread::new(c)
            .priority(Priority::new(1))
            .stack(Stack::try_new_in(c, 512).unwrap())
            .name("Heartbeat\0")
            .spawn(move || {
                loop {
                    state_ok.set_on();
                    sleep(100);
                    state_ok.set_off();
                    sleep(900);
                }
            });

        let ctrl_req_recv = ctrl_req.clone();
        let mut state_boiler = state.error;
        let flow_ctrl = flow.clone();
        let shot_time_ctrl = shot_time.clone();
        let suspend_control_send = suspend_control.clone();
        Thread::new(c)
            .priority(Priority::new(0))
            .stack(Stack::try_new_in(c, 1024).unwrap())
            .name("Control\0")
            .spawn(move || {
                let mut state = EspressoState::Off;
                let mut start_time = 0;
                loop {
                    match state {
                        EspressoState::Off => {
                            // just to be safe
                            actuators.stop_pump();
                            actuators.close_valve();

                            if buttons.is_water_pressed() {
                                state = EspressoState::Idle;
                                ui_suspend_sender.send(false).ok();
                                suspend_control_send.add_permits(1);
                                info!("Turning espresso machine on.");
                            }
                        }
                        EspressoState::Idle => {
                            if !buttons.is_water_pressed() {
                                state = EspressoState::Off;
                                ui_suspend_sender.send(true).ok();
                                info!("Turning espresso machine off.");
                            } else if buttons.is_brew_pressed() {
                                flow_ctrl.reset();
                                actuators.start_pump();
                                actuators.open_valve();
                                start_time = syscall::tick_count();

                                state = EspressoState::PullShot;
                                info!("Start pulling espresso shot.");
                            }
                        }
                        EspressoState::PullShot => {
                            // Stop at
                            //   (- 40ml)
                            //   (- 27s)
                            //   - Switch released
                            if /*flow_ctrl.volume() >= 40.0 ||*/
                            /*(syscall::tick_count() - start_time) >= 27_000 ||*/
                                !buttons.is_brew_pressed()
                            {
                                actuators.stop_pump();
                                actuators.close_valve();

                                state = EspressoState::WaitForRelease;
                                info!("Finished espresso shot.");
                            }

                            if let Ok(mut t) = shot_time_ctrl.try_lock() {
                                *t = (syscall::tick_count() - start_time) as f32 / 1000.0;
                            }
                        }
                        EspressoState::WaitForRelease => {
                            if !buttons.is_brew_pressed() {
                                state = EspressoState::Idle;
                            }
                        }
                    }

                    // use steam switch as controller override
                    if buttons.is_steam_pressed() && !(state == EspressoState::Off) {
                        if let Ok(ctrl) = ctrl_req_recv.try_lock() {
                            if ctrl.boiler {
                                actuators.set_boiler_on();
                                state_boiler.set_on();
                            } else {
                                actuators.set_boiler_off();
                                state_boiler.set_off();
                            }
                        }
                    } else {
                        actuators.set_boiler_off();
                        state_boiler.set_off();
                    }

                    sleep(50);
                }
            });

        let meas_ctrl = meas.clone();
        Thread::new(c)
            .priority(Priority::new(1))
            .stack(Stack::try_new_in(c, 2048).unwrap())
            .name("T Control\0")
            .spawn(move || {
                let setpoint = 98.0_f32;
                let system = state_space::System {
                    A: [[-83.3e-3, 8.8e-3, 0.0, 0.0],
                        [83.3e-3, -38.2e-3, 55.6e-3, 0.0],
                        [0.0, 29.4e-3, -62.2e-3, 1.8e-3],
                        [0.0, 0.0, 6.7e-3, -1.98e-3]],
                    B: [[1.0], [0.0], [0.0], [0.0]],
                    C: [[8.33e-3, 0.0, 0.0, 0.0],
                        [0.0, 881.8e-6, 0.0, 0.0],
                        [0.0, 0.0, 1.67e-3, 0.0],
                        [0.0, 0.0, 0.0, 444.4e-6]],
                    D: [[0.0, 0.0, 0.0, 0.0]],
                };

                let mut observer = state_space::Observer::new(
                    system.clone(),
                    [[0.0, 0.0, 0.0, 0.0],
                        [0.0, 100.0, 0.0, 0.0],
                        [0.0, 0.0, 500.0, 0.0],
                        [0.0, 0.0, 0.0, 200.0]],
                    0.5,
                );
                let controller = state_space::Controller::new(
                    [[59.8e-3, 838.6e-3, 307.2e-3, 21.6e-3]],
                    [setpoint],
                    [1180.0],
                );

                sleep(5000);
                match meas_ctrl.lock(0) {
                    Ok(m) => {
                        observer.init_state([
                            m.temperature.boiler_inside/system.C[0][0],
                            m.temperature.boiler_inside/system.C[1][1],
                            m.temperature.boiler_outside/system.C[2][2],
                            m.temperature.group_head_outside/system.C[3][3]
                        ]);
                    }
                    Err(_) => {}
                }

                let mut output = [0.0_f32; 1];
                loop {
                    if let Ok(m) = meas_ctrl.try_lock() {
                        // reset observer if requested
                        if let Ok(p) = suspend_control.try_acquire() {
                            observer.init_state([
                                m.temperature.boiler_inside/system.C[0][0],
                                m.temperature.boiler_inside/system.C[1][1],
                                m.temperature.boiler_outside/system.C[2][2],
                                m.temperature.group_head_outside/system.C[3][3]
                            ]);
                            p.forget();
                            info!("Reset control observer.");
                        }

                        observer.update(
                            &output,
                            &[0.0, // we don't know that
                                 m.temperature.boiler_inside,
                                 m.temperature.boiler_outside,
                                 m.temperature.group_head_outside]);
                        let x_est = observer.x();
                        controller.update(&x_est, &mut output);
                        // Limit output power, we can only switch the heater on
                        //(1.2kW) or off (0W).
                        output[0] =  if output[0] > 600.0 { 1200.0 } else { 0.0 };

                        if let Ok(mut ctrl) = ctrl_req.try_lock() {
                            ctrl.boiler = output[0] > 600.0;
                        }

                        log_ctrl_sender.send(ControllerData {
                            x: m.temperature.boiler_inside,
                            e: setpoint - m.temperature.boiler_inside,
                            y: output[0],
                            internal_1: x_est[0] * system.C[0][0],
                            internal_2: x_est[1] * system.C[1][1],
                            internal_3: x_est[2] * system.C[2][2],
                            internal_4: x_est[3] * system.C[3][3],
                        }).ok();
                    }

                    sleep(500);
                }
            });


        let adc = Ad7124::new(spi).unwrap();
        let channel_config = Config {
            rtd_channels: [
                Some(RTDChannel {
                    coeff: CALLENDAR_VAN_DUSEN_IEC60751,
                }),
                Some(RTDChannel {
                    coeff: CALLENDAR_VAN_DUSEN_IEC60751,
                }),
                Some(RTDChannel {
                    coeff: CALLENDAR_VAN_DUSEN_IEC60751,
                }),
            ],
            current: ExcitationCurrent::_250uA,
            gain: Gain::_32,
        };
        let mut sense_rtd = Ad7124Rtd3W::new(adc, channel_config, CIRCUIT).unwrap();

        let flow_log = flow.clone();
        Thread::new(c)
            .priority(Priority::new(2))
            .stack(Stack::try_new_in(c, 4096).unwrap())
            .name("Sense\0")
            .spawn(move || {
                sense_rtd.calibrate().unwrap();
                sense_rtd.set_active_channel(0).unwrap();

                loop {
                    // measure
                    let water_pressure = analog_sensors.read_pressure().unwrap();

                    let t_boiler_inside = block_or_sleep!(sense_rtd.read_temperature()).unwrap_or(0.0_f32);
                    sense_rtd.set_active_channel(1).ok();
                    let t_boiler_outside = block_or_sleep!(sense_rtd.read_temperature()).unwrap_or(0.0_f32);
                    sense_rtd.set_active_channel(2).ok();
                    let t_group_head_outside = block_or_sleep!(sense_rtd.read_temperature()).unwrap_or(0.0_f32);
                    sense_rtd.set_active_channel(0).ok();

                    // Update global data
                    if let Ok(mut m) = meas.try_lock() {
                        m.temperature.boiler_inside = t_boiler_inside;
                        m.temperature.boiler_outside = t_boiler_outside;
                        m.temperature.group_head_inside = 0.0;
                        m.temperature.group_head_outside = t_group_head_outside;
                        m.water_pressure = water_pressure;
                        m.water_volume = flow_log.volume();

                        if let Ok(t) = shot_time.try_lock() {
                            m.shot_time = *t;
                        }

                        // Send to UI
                        meas_sender.send(*m).ok();
                    } else {
                        warn!("Shared measurement locked!");
                    }

                    // Send to logger
                    if log_sender.send(MeasData {
                        t: syscall::tick_count() as u32,
                        temperature: MeasTemperature {
                            boiler_inside: t_boiler_inside,
                            boiler_outside: t_boiler_outside,
                            group_head_outside: t_group_head_outside,
                            group_head_inside: 0.0,
                        },
                        water_pressure,
                        water_volume: flow_log.volume(),
                        shot_time: 0.0,
                    }).is_err() {
                        warn!("Log queue is full!");
                    }

                    sleep(50);
                }
            });

        InterruptHandler::new(&c)
            .stack(InterruptStack::Kernel)
            .connect_interrupt(flow_sensor::irqn())
            .handler(move |_c| {
                flow.inc_update();
            });
    })
}

