use embedded_hal::blocking::spi::Transfer;
use stm32f7xx_hal::gpio::{Alternate, EPin, Output, PushPull, Pin};

use stm32f7xx_hal::spi;
use stm32f7xx_hal::pac::{SPI2};
use stm32f7xx_hal::rcc::{APB1, Clocks};
use stm32f7xx_hal::spi::Enabled;
use stm32f7xx_hal::prelude::*;

type SpiInstance = spi::Spi<SPI2, (Pin<'I', 1, Alternate<5>>, Pin<'C', 2, Alternate<5>>, Pin<'I', 3, Alternate<5>>), Enabled<u8>>;

pub struct SpiPins {
    pub sclk: Pin<'I', 1, Alternate<5>>,
    pub miso: Pin<'C', 2, Alternate<5>>,
    pub mosi: Pin<'I', 3, Alternate<5>>,
}

pub struct SenseBus {
    spi: SpiInstance,
    cs_pins: [EPin<Output<PushPull>>; 4],
    pub sync_pin: EPin<Output<PushPull>>,
}


impl SenseBus {
    pub fn new(
        spi_pins: SpiPins,
        cs_pins: [EPin<Output<PushPull>>; 4],
        mut sync_pin: EPin<Output<PushPull>>,
        spi: SPI2,
        clocks: &Clocks,
        apb1: &mut APB1
    ) -> Self {

        let sense_spi = spi::Spi::new(
            spi,
            (spi_pins.sclk, spi_pins.miso, spi_pins.mosi)
        );

        let sense_spi = sense_spi.enable::<u8>(
            spi::Mode {
                polarity: spi::Polarity::IdleHigh,
                phase: spi::Phase::CaptureOnSecondTransition,
            },
            500_u32.kHz(),
            clocks,
            apb1
        );

        sync_pin.set_high();
        SenseBus {
            spi: sense_spi,
            cs_pins,
            sync_pin,
        }
    }
}

impl Transfer<u8> for SenseBus {
    type Error = spi::Error;

    fn transfer<'w>(&mut self, words: &'w mut [u8]) -> Result<&'w [u8], Self::Error> {
        self.cs_pins[2].set_low();  // RTD module
        //self.cs_pins[0].set_low();  // Thermocouple module

        let res = self.spi.transfer(words);

        self.cs_pins[2].set_high();  // RTD module
        //self.cs_pins[0].set_high();  // Thermocouple module
        res
    }
}
