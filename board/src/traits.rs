pub trait Actuators {
    fn start_pump(&mut self);
    fn stop_pump(&mut self);
    fn is_pump_running(&self) -> bool;

    fn open_valve(&mut self);
    fn close_valve(&mut self);
    fn is_vale_open(&self) -> bool;

    fn set_boiler_on(&mut self);
    fn set_boiler_off(&mut self);
    fn is_boiler_on(&self) -> bool;
}

pub trait Led {
    fn set_on(&mut self);
    fn set_off(&mut self);
    fn toggle(&mut self);
}

pub trait Buttons {
    fn is_brew_pressed(&self) -> bool;
    fn is_water_pressed(&self) -> bool;
    fn is_steam_pressed(&self) -> bool;
}