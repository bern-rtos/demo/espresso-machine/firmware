use stm32f7xx_hal::gpio::{Analog, Pin};
use stm32f7xx_hal::pac::ADC1;
use stm32f7xx_hal::rcc::{APB2, Clocks};
use stm32f7xx_hal::adc::Adc;
use stm32f7xx_hal::prelude::*;

const N_BITS: u8 = 12;

pub struct Pins {
    pub pressure: Pin<'B', 1, Analog>,
}

pub struct AnalogSensors {
    pins: Pins,
    adc: Adc<ADC1>,
}


impl AnalogSensors {
    pub fn new(pins: Pins, adc1: ADC1, clocks: &Clocks, apb2: &mut APB2) -> AnalogSensors {
        let mut adc = Adc::adc1(adc1, apb2, clocks, N_BITS, true);
        adc.default_cfg();

        AnalogSensors {
            pins,
            adc,
        }
    }

    pub fn read_pressure(&mut self) -> Result<f32, nb::Error<()>> {
        self.adc.read(&mut self.pins.pressure).map(|raw: u32| {
            let norm = raw as f32 / (2_u32.pow(N_BITS as u32) - 1) as f32;
            let bar = ((norm - 0.1_f32) * 17.2_f32 - 0.0_f32) / 0.8_f32;
            if bar < 0.0_f32 {
                0.0_f32
            } else {
                bar
            }
        })
    }
}