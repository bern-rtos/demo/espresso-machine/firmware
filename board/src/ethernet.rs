use stm32_eth::{Eth, EthPins, RingEntry, RxDescriptor, RxRingEntry, TxDescriptor, TxRingEntry};
use stm32_eth::stm32::ETHERNET_DMA;
use stm32f7xx_hal::gpio::{Alternate, Floating, Input, Output, PA7, PB12, PB13, PC4, PC5, PG11, Pin};
use stm32f7xx_hal::pac::ETHERNET_MAC;
use stm32f7xx_hal::rcc::Clocks;

use stm32f7xx_hal::gpio::PA1;


pub struct Pins {
    pub ref_clk: PA1<Input<Floating>>,
    pub crs: PA7<Input<Floating>>,
    pub tx_en: PG11<Input<Floating>>,
    pub tx_d0: PB12<Input<Floating>>,
    pub tx_d1: PB13<Input<Floating>>,
    pub rx_d0: PC4<Input<Floating>>,
    pub rx_d1: PC5<Input<Floating>>,

    pub mdio: Pin<'A', 2, Alternate<11>>,
    pub mdclk: Pin<'C', 1, Alternate<11>>,

    pub reset: Pin<'C', 0, Output>,
    pub mco: Pin<'A', 8, Alternate<0>>,
}

pub struct SmiPins {
    pub mdio: Pin<'A', 2, Alternate<11>>,
    pub mdclk: Pin<'C', 1, Alternate<11>>,
}


pub struct Ethernet {
    reset: Pin<'C', 0, Output>,
    pub eth: Eth<'static, 'static>,
    pub smi_pins: SmiPins,
}

const RX_RING_DEFAULT: RxRingEntry = RingEntry::<RxDescriptor>::new();
const TX_RING_DEFAULT: TxRingEntry = RingEntry::<TxDescriptor>::new();
//#[link_section=".process.log"]
static mut RX_RING: [RxRingEntry; 8] = [RX_RING_DEFAULT; 8];
//#[link_section=".process.log"]
static mut TX_RING: [TxRingEntry; 4] = [TX_RING_DEFAULT; 4];

impl Ethernet {
    pub fn new(pins: Pins, mac: ETHERNET_MAC, dma: ETHERNET_DMA, clocks: Clocks) -> Ethernet {
        let _ = pins.mco;
        let mdio = pins.mdio;
        let mdclk = pins.mdclk;
        let reset = pins.reset;

        // There seems to be a problem with the init. of the buffers proc. mem.
        // So we reinit here for safety.
        unsafe {
            for entry in RX_RING.iter_mut() {
                *entry = RX_RING_DEFAULT;
            }
            for entry in TX_RING.iter_mut() {
                *entry = TX_RING_DEFAULT;
            }
        }

        let eth_pins = EthPins {
            ref_clk: pins.ref_clk,
            crs: pins.crs,
            tx_en: pins.tx_en,
            tx_d0: pins.tx_d0,
            tx_d1: pins.tx_d1,
            rx_d0: pins.rx_d0,
            rx_d1: pins.rx_d1,
        };
        let eth = Eth::new(
            mac,
            dma,
            unsafe { &mut RX_RING[..] },
            unsafe { &mut TX_RING[..] },
            clocks,
            eth_pins,
        ).unwrap();

        let smi_pins = SmiPins {
            mdio,
            mdclk
        };

        Ethernet {
            reset,
            eth,
            smi_pins,
        }
    }

    pub fn irqn(&self) -> u16 {
        stm32f7xx_hal::interrupt::ETH as u16
    }

    pub fn enable(&mut self) {
        self.reset.set_high();
        self.eth.interrupt_handler();
        self.eth.enable_interrupt();
    }
}

pub unsafe fn handle_interrupt() {
    let p = stm32f7xx_hal::pac::Peripherals::steal();
    stm32_eth::eth_interrupt_handler(&p.ETHERNET_DMA);
}