use stm32f7xx_hal::{
    pac::{DMA2D, LTDC},
    ltdc::{DisplayController, PixelFormat},
    rcc::{HSEClock, HSEClockMode},
};
use stm32f7xx_hal::gpio::{EPin, Output};
use stm32f7xx_hal::prelude::*;

mod riverdi;
use riverdi::RIVERDI_RVT50HQTNWC00;

pub struct Display {
    pub controller: DisplayController<u32>,
    disp_on: EPin<Output>,
}

impl Display {
    pub fn new(disp_on: EPin<Output>, ltdc: LTDC, dma2d: DMA2D) -> Self {
        let controller = DisplayController::new(
            ltdc,
            dma2d,
            PixelFormat::ARGB8888,
            RIVERDI_RVT50HQTNWC00,
            Some(&HSEClock::new(25.MHz(), HSEClockMode::Oscillator)),
        );

        Display {
            controller,
            disp_on
        }
    }

    pub fn enable(&mut self) {
        self.disp_on.set_high();
    }

    pub fn disable(&mut self) {
        self.disp_on.set_low();
    }
}