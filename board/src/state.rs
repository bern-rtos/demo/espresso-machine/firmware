use stm32f7xx_hal::gpio::{EPin, Output, PushPull};

pub struct Pins {
    pub ok: EPin<Output<PushPull>>,
    pub error: EPin<Output<PushPull>>,
    pub comm: EPin<Output<PushPull>>,
}

pub struct State {
    pub ok: Led,
    pub error: Led,
    pub comm: Led,
}


impl State {
    pub fn new(mut pins: Pins) -> Self {
        pins.ok.set_low();
        pins.error.set_low();
        pins.comm.set_low();

        State {
            ok: Led::new(pins.ok),
            error: Led::new(pins.error),
            comm: Led::new(pins.comm),
        }
    }
}

pub struct Led {
    pin: EPin<Output<PushPull>>,
}

impl Led {
    fn new(pin: EPin<Output<PushPull>>) -> Self {
        Led {
            pin
        }
    }
}

impl crate::traits::Led for Led {
    fn set_on(&mut self) {
        self.pin.set_high();
    }

    fn set_off(&mut self) {
        self.pin.set_low();
    }

    fn toggle(&mut self) {
        self.pin.toggle();
    }
}