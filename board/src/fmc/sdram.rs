pub(crate) mod is42s16160g;

use stm32_fmc::{Sdram, SdramTargetBank};
use stm32f7xx_hal::{
    fmc::{
        FMC,
        FmcExt,
    },
    rcc::Clocks,
    pac
};
use is42s16160g::Is42s16160g;

pub fn new(fmc: pac::FMC, clocks: &Clocks) -> Sdram<FMC, Is42s16160g> {
    fmc.sdram_unchecked(
        SdramTargetBank::Bank1,
        Is42s16160g {},
        clocks
    )
}

