mod sdram;

use stm32f7xx_hal::pac;
use stm32f7xx_hal::gpio::{Pin, Alternate};
use stm32f7xx_hal::rcc::Clocks;
use super::BoardDelay;

pub struct Pins {
    pub a0: Pin<'F', 0, Alternate<12>>,
    pub a1: Pin<'F', 1, Alternate<12>>,
    pub a2: Pin<'F', 2, Alternate<12>>,
    pub a3: Pin<'F', 3, Alternate<12>>,
    pub a4: Pin<'F', 4, Alternate<12>>,
    pub a5: Pin<'F', 5, Alternate<12>>,
    pub a6: Pin<'F', 12, Alternate<12>>,
    pub a7: Pin<'F', 13, Alternate<12>>,
    pub a8: Pin<'F', 14, Alternate<12>>,
    pub a9: Pin<'F', 15, Alternate<12>>,
    pub a10: Pin<'G', 0, Alternate<12>>,
    pub a11: Pin<'G', 1, Alternate<12>>,
    pub a12: Pin<'G', 2, Alternate<12>>,

    pub d0: Pin<'D', 14, Alternate<12>>,
    pub d1: Pin<'D', 15, Alternate<12>>,
    pub d2: Pin<'D', 0, Alternate<12>>,
    pub d3: Pin<'D', 1, Alternate<12>>,
    pub d4: Pin<'E', 7, Alternate<12>>,
    pub d5: Pin<'E', 8, Alternate<12>>,
    pub d6: Pin<'E', 9, Alternate<12>>,
    pub d7: Pin<'E', 10, Alternate<12>>,
    pub d8: Pin<'E', 11, Alternate<12>>,
    pub d9: Pin<'E', 12, Alternate<12>>,
    pub d10: Pin<'E', 13, Alternate<12>>,
    pub d11: Pin<'E', 14, Alternate<12>>,
    pub d12: Pin<'E', 15, Alternate<12>>,
    pub d13: Pin<'D', 8, Alternate<12>>,
    pub d14: Pin<'D', 9, Alternate<12>>,
    pub d15: Pin<'D', 10, Alternate<12>>,

    pub ba0: Pin<'G', 4, Alternate<12>>,
    pub ba1: Pin<'G', 5, Alternate<12>>,

    pub nbl0: Pin<'E', 0, Alternate<12>>,
    pub nbl1: Pin<'E', 1, Alternate<12>>,
    pub sdncas: Pin<'G', 15, Alternate<12>>,
    pub sdclk: Pin<'G', 8, Alternate<12>>,
    pub sdnwe: Pin<'H', 5, Alternate<12>>,
    pub sdne0: Pin<'H', 3, Alternate<12>>,
    pub sdcke0: Pin<'H', 2, Alternate<12>>,
    pub sdnras: Pin<'F', 11, Alternate<12>>,
}

pub struct Fmc {
    pub pins: Pins,
    pub fmc: pac::FMC,
}

#[macro_export]
macro_rules! into_fmc_pin {
    ($($pin:expr),*) => {
        (
            $(
                $pin.into_push_pull_output()
                    .set_speed(Speed::VeryHigh)
                    .into_alternate::<12>()
                    .internal_pull_up(true)
            ),*
        )
    };
}



impl Fmc {
    pub fn sdram(self, clocks: &Clocks, delay: &mut BoardDelay) {
        let mut sdram = sdram::new(self.fmc, clocks);
        sdram.init(delay);
    }
}