use stm32f7xx_hal::gpio::{EPin, Input, PullUp};

pub struct Pins {
    pub brew: EPin<Input<PullUp>>,
    pub water: EPin<Input<PullUp>>,
    pub steam: EPin<Input<PullUp>>,
}

pub struct Buttons {
    pins: Pins,
}


impl Buttons {
    pub fn new(pins: Pins) -> Self {
        Buttons {
            pins,
        }
    }
}


impl crate::traits::Buttons for Buttons {
    fn is_brew_pressed(&self) -> bool {
        self.pins.brew.is_low()
    }

    fn is_water_pressed(&self) -> bool {
        self.pins.water.is_low()
    }

    fn is_steam_pressed(&self) -> bool {
        self.pins.steam.is_low()
    }
}