use stm32f7xx_hal::gpio::{EPin, Input, Output, PullUp, PushPull};

pub struct Pins {
    pub pump: EPin<Output<PushPull>>,
    pub valve: EPin<Output<PushPull>>,
    pub user: EPin<Output<PushPull>>,
    pub boiler: EPin<Output<PushPull>>,
    pub zero_crossing: EPin<Input<PullUp>>,
}

pub struct Actuators {
    pins: Pins
}

impl Actuators {
    pub fn new(mut pins: Pins) -> Self {
        pins.pump.set_low();
        pins.valve.set_low();
        pins.user.set_low();
        pins.boiler.set_low();

        Actuators {
            pins,
        }
    }
}

// todo: sync to zero crossing
impl crate::traits::Actuators for Actuators {
    fn start_pump(&mut self) {
        self.pins.pump.set_high();
    }

    fn stop_pump(&mut self) {
        self.pins.pump.set_low();
    }

    fn is_pump_running(&self) -> bool {
        self.pins.pump.is_set_high()
    }


    fn open_valve(&mut self) {
        self.pins.valve.set_high();
    }

    fn close_valve(&mut self) {
        self.pins.valve.set_low();
    }

    fn is_vale_open(&self) -> bool {
        self.pins.valve.is_set_high()
    }


    fn set_boiler_on(&mut self) {
        self.pins.boiler.set_high();
    }

    fn set_boiler_off(&mut self) {
        self.pins.boiler.set_low();
    }

    fn is_boiler_on(&self) -> bool {
        self.pins.boiler.is_set_high()
    }
}