# Converting an image to ARGB8888

1. Convert image on [LVGLs website](https://lvgl.io/tools/imageconverter) with the following settings:
   1. True color with alpha
   2. Binary RGB888
2. Convert binary to C array:
   ```
   xxd -i img.bin img.c
   ```
3. Change C header to Rust module.