//! Telegraf example
//! copied from <https://github.com/quartiq/stabilizer/blob/964d0fbd9d89f81c33441a8a5fe4f616fe840657/src/hardware/setup.rs>

#![no_std]
#![no_main]

use core::cell::RefCell;
use panic_rtt_target as _;
use rtt_target::{rtt_init_print, rprintln};

use core::fmt::Write;
use embedded_hal::prelude::_embedded_hal_blocking_delay_DelayMs;

use smoltcp::iface::SocketHandle;
use smoltcp::socket::TcpSocket;
use smoltcp::wire::{IpEndpoint, Ipv4Address};
use smoltcp::time::Instant;
use smoltcp::wire::IpAddress;

use stm32f7xx_hal::interrupt;
use board::Board;


static ETH_PENDING: cortex_m::interrupt::Mutex<RefCell<bool>> = cortex_m::interrupt::Mutex::new(RefCell::new(false));


const N_TCP_SOCKETS: usize = 1;

pub struct NetStorage {
    pub ip_addrs: [smoltcp::wire::IpCidr; 1],

    // Note: There is an additional socket set item required for the DHCP socket.
    pub sockets: [smoltcp::iface::SocketStorage<'static>; N_TCP_SOCKETS + 1],
    pub tcp_socket_storage: [TcpSocketStorage; N_TCP_SOCKETS],
    pub neighbor_cache: [Option<(IpAddress, smoltcp::iface::Neighbor)>; 8],
    pub routes_cache: [Option<(smoltcp::wire::IpCidr, smoltcp::iface::Route)>; 8],
}

#[derive(Copy, Clone)]
pub struct TcpSocketStorage {
    rx_storage: [u8; 1024],
    tx_storage: [u8; 1024],
}

impl TcpSocketStorage {
    const fn new() -> Self {
        Self {
            rx_storage: [0; 1024],
            tx_storage: [0; 1024],
        }
    }
}

//#[link_section=".process.log"]
static mut NETWORK_STORAGE: NetStorage = NetStorage {
    ip_addrs: [smoltcp::wire::IpCidr::Ipv6(
        smoltcp::wire::Ipv6Cidr::SOLICITED_NODE_PREFIX,
    )],
    neighbor_cache: [None; 8],
    routes_cache: [None; 8],
    sockets: [smoltcp::iface::SocketStorage::EMPTY; N_TCP_SOCKETS + 1],
    tcp_socket_storage: [TcpSocketStorage::new(); N_TCP_SOCKETS],
};


struct NetConfig {
    pub ip: IpAddress,
    pub netmask: u8,
    pub mac: smoltcp::wire::EthernetAddress,
}

const NETCONFIG: NetConfig = NetConfig {
    ip: IpAddress::Ipv4(Ipv4Address::new(192,168,10,90)),
    netmask: 24,
    mac:  smoltcp::wire::EthernetAddress([0x00, 0x00, 0xDE, 0xAD, 0xBE, 0xEF]),
};


#[cortex_m_rt::entry]
fn main() -> ! {
    let board = Board::new();

    rprintln!("Bern Espresso - Ethernet example");

    let mut eth = board.ethernet.eth;
    let clock = board.sys_clock;
    let mut delay = board.delay;

    let store = unsafe { &mut NETWORK_STORAGE };
    store.ip_addrs[0] = smoltcp::wire::IpCidr::new(NETCONFIG.ip, NETCONFIG.netmask);

    let mut routes = smoltcp::iface::Routes::new(&mut store.routes_cache[..]);
    //routes
    //    .add_default_ipv4_route(smoltcp::wire::Ipv4Address::UNSPECIFIED)
    //    .unwrap();

    let neighbor_cache = smoltcp::iface::NeighborCache::new(&mut store.neighbor_cache[..]);

    let mut iface = smoltcp::iface::InterfaceBuilder::new(
        &mut eth,
        &mut store.sockets[..],
    )
        .hardware_addr(smoltcp::wire::HardwareAddress::Ethernet(NETCONFIG.mac))
        .neighbor_cache(neighbor_cache)
        .ip_addrs(&mut store.ip_addrs[..])
        .routes(routes)
        .finalize();

    let mut sockets_tcp = [SocketHandle::default(); N_TCP_SOCKETS];
    for (i, storage) in store.tcp_socket_storage[..].iter_mut().enumerate() {
        let tcp_socket = {
            let rx_buffer = smoltcp::socket::TcpSocketBuffer::new(
                &mut storage.rx_storage[..],
            );
            let tx_buffer = smoltcp::socket::TcpSocketBuffer::new(
                &mut storage.tx_storage[..],
            );

            TcpSocket::new(rx_buffer, tx_buffer)
        };

        sockets_tcp[i] = iface.add_socket(tcp_socket);
    }

    let telegraf_socket = sockets_tcp[0];

    loop {
        let now_ms = clock.ticks();
        match iface.poll(Instant::from_millis(clock.ticks())) {
            Ok(true) => {
                rprintln!("try to send a message");

                let (socket, cx) = iface.get_socket_and_context::<TcpSocket>(telegraf_socket);

                // open
                if !socket.is_open() {
                    rprintln!("socket closed");
                    socket.connect(cx,
                                   IpEndpoint::new(IpAddress::Ipv4(Ipv4Address::new(192, 168, 10, 10)), 8094),
                                   IpEndpoint::new(IpAddress::Unspecified, 1234)
                    ).unwrap();
                }

                // it could take a few iterations until the socket is opened
                if socket.can_send() {
                    // send and close
                    write!(socket, "{{\"t\":{}}}\n", now_ms)
                        .map(|_| {
                            socket.close();
                        })
                        .or_else(|e| {
                            rprintln!("TCP send error: {:?}", e);
                            Err(e)
                        })
                        .unwrap();
                } else {
                    rprintln!("socket cannot send");
                }
            }
            Ok(false) => {
                cortex_m::interrupt::free(|cs| {
                    let eth_pending = ETH_PENDING.borrow(cs).borrow_mut();
                    if !*eth_pending {
                        //cortex_m::asm::wfi();
                        // Awaken by interrupt
                    }
                });
            }
            Err(e) =>
            // Ignore malformed packets
            {
               rprintln!("Error: {:?}", e)
            }
        }
    }
}


#[allow(non_snake_case)]
#[interrupt]
fn ETH() {
    cortex_m::interrupt::free(|cs| {
        let mut eth_pending = ETH_PENDING.borrow(cs).borrow_mut();
        *eth_pending = true;
    });

    unsafe {
        board::ethernet::handle_interrupt();
    }
}