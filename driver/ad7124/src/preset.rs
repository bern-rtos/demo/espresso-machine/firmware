pub mod rtd_3wire;
pub mod thermocouple;

/// Coefficients for linear equation.
/// ```ignore
/// y(x) = a_0 + a_1 * x
/// ```
pub struct LinearCoeff {
    pub a_0: f32,
    pub a_1: f32,
}

/// Callendar-Van Dusen coefficients for RTD model.
///
/// For `T` in 0°C..661°C:
/// ```ignore
/// R(T) = r0 * (1 + a * T + b * T^2)
/// ```
///
/// For `T` in -200°C..0°C:
/// ```ignore
/// R(T) = r0 * (1 + a * T + b * T^2 + c * (T - 100°C) * T^3
/// ```
pub struct CallendarVanDusenCoeff {
    pub r0: f32,
    pub a: f32,
    pub b: f32,
    pub c: f32,
}

/// Callendar-Van Dusen coefficients according to the IEC60751 standard for
/// industrial RTD sensors.
pub const CALLENDAR_VAN_DUSEN_IEC60751: CallendarVanDusenCoeff = CallendarVanDusenCoeff {
    r0: 100.0,
    a: 3.9083e-3,
    b: -5.775e-7,
    c: -4.183e-12,
};