#![no_std]

pub mod register;
pub mod preset;

use core::fmt::Debug;
use core::sync::atomic::{compiler_fence, Ordering};
use register::*;
use embedded_hal::blocking::spi::Transfer;
use crate::preset::CallendarVanDusenCoeff;
use micromath::F32Ext;

pub const N_BIT: u32 = 24;
pub const N_CHANNEL: u8 = 8;
pub const INTERNAL_REF_VOLTAGE: f32 = 2.5;
const N_CONN_ATTEMPTS: u8 = 5;

#[derive(Debug)]
pub enum Error<E> {
    /// SPI bus error.
    SPI(E),
    /// Chip ID not valid.
    InvalidId,
    /// One or more method arguments are out of bounds.
    ArgumentOutOfBounds,
    /// The measurement is out of sensible bounds.
    InvalidMeasurement,
}

#[allow(non_camel_case_types)]
pub enum Id {
    /// Common AD7124-8.
    AD7124_8,
    /// AD7124-8 B Grade (higher precision).
    AD7124_8_B,
}

pub struct Ad7124<Spi> {
    spi: Spi,
}

impl<Spi, E> Ad7124<Spi>
    where
        Spi: Transfer<u8, Error = E>,
        E: Debug,
{
    pub fn new(spi: Spi) -> Result<Self, Error<E>> {
        let mut adc = Ad7124 {
            spi,
        };

        adc.reset()?;

        // Read ID to check whether comms and chip are working.
        // Experiance showed that sometime multiple attemps are necessary for
        // the chip to respond (especially with a release build).
        for i in 0..N_CONN_ATTEMPTS {
            let res = adc.id();
            if res.is_ok() {
                break;
            } else if i == N_CONN_ATTEMPTS - 1 {
                return Err(Error::InvalidId);
            }
        }

        Ok(adc)
    }

    pub fn reset(&mut self) -> Result<(), Error<E>> {
        let mut reset_pattern = [0xFFu8; 8];
        self.spi.transfer(&mut reset_pattern).map_err(Error::SPI)?;

        while self.read_register::<STATUS>()?.POR_FLAG() {
            compiler_fence(Ordering::SeqCst);
        }
        Ok(())
    }

    pub fn id(&mut self) -> Result<Id, Error<E>> {
        let id: ID = self.read_register()?;
        match (id.DEVICE_ID(), id.SILICON_REVISION()) {
            (0x1, 0x4) => Ok(Id::AD7124_8),
            (0x1, 0x6) => Ok(Id::AD7124_8_B),
            (_, _) => Err(Error::InvalidId),
        }
    }

    pub fn read_measurement(&mut self) -> nb::Result<u32, Error<E>> {
        let stat = self.read_register::<STATUS>().map_err(nb::Error::Other)?;
        if stat.RDY() {
            return Err(nb::Error::WouldBlock);
        }

        let data = self.read_register::<DATA>()?;
        Ok(data.VALUE())
    }

    /// Run full-scale and zero-scale calibration on one channel.
    ///
    /// **Note:**
    ///  - Channel must be fully setup.
    ///  - All channels must be disabled beforehand.
    pub fn full_internal_calibration(&mut self, channel: u8) -> Result<(), Error<E>> {
        if channel >= N_CHANNEL {
            return Err(Error::ArgumentOutOfBounds);
        }

        let ctrl_backup = self.read_register::<ADC_CONTROL>()?;
        let cfg_backup = self.read_register_offset::<CONFIG>(channel)?;
        let ch_backup = self.read_register_offset::<CHANNEL>(channel)?;

        // Reset offset before calibration
        let mut offset = unsafe { OFFSET::new(channel as u8).unwrap_unchecked() };
        offset.set_VALUE(0x800000);
        self.write_register(offset)?;

        // Activate channel
        let mut ch = ch_backup.clone();
        ch.set_ENABLE(true);
        self.write_register(ch)?;
        let mut cfg = cfg_backup.clone();
        cfg.set_REF_SEL(Ref::Internal);
        self.write_register(cfg)?;

        // Start full-scale calibration
        let mut ctrl = ctrl_backup.clone();
        if ctrl.POWER_MODE() == PowerMode::FullPower {
            // Full scale calibration only works in low and medium power.
            ctrl.set_POWER_MODE(PowerMode::MidPower);
        }
        ctrl.set_MODE(OperationMode::CalIntFullScale);
        self.write_register(ctrl)?;
        // Wait for calibration to finish
        while self.read_register::<ADC_CONTROL>()?.MODE() != OperationMode::Idle {
            compiler_fence(Ordering::SeqCst);
        }

        // Start zero-scale calibration
        let mut ctrl = ctrl_backup.clone();
        ctrl.set_MODE(OperationMode::CalIntZeroScale);
        self.write_register(ctrl)?;
        // Wait for calibration to finish
        while self.read_register::<ADC_CONTROL>()?.MODE() != OperationMode::Idle {
            compiler_fence(Ordering::SeqCst);
        }

        // Restore previous config
        self.write_register(ch_backup)?;
        self.write_register(cfg_backup)?;
        self.write_register(ctrl_backup)?;
        Ok(())
    }

    pub fn read_register<R>(&mut self) -> Result<R, Error<E>>
        where R: Address + Size + Deserialize
    {
        let mut data = [0; 5];
        let mut comms = COMMS::new();
        comms.set_WEN(false);
        comms.set_RW(true);
        comms.set_RS(R::addr());
        data[0] = comms.bits();

        self.spi.transfer(&mut data[..R::size() + 1]).map_err(Error::SPI)?;
        Ok(unsafe { R::from_bytes(&data[1..R::size() + 1]) })
    }

    pub fn read_register_offset<R>(&mut self, offset: u8) -> Result<R, Error<E>>
        where R: AddressOffset + Size + DeserializeOffset
    {
        let mut data = [0; 5];
        let mut comms = COMMS::new();
        comms.set_WEN(false);
        comms.set_RW(true);
        comms.set_RS(R::addr(offset).ok_or(Error::ArgumentOutOfBounds)?);
        data[0] = comms.bits();

        self.spi.transfer(&mut data[..R::size() + 1]).map_err(Error::SPI)?;
        Ok(unsafe { R::from_bytes(&data[1..R::size() + 1], offset) }
            .ok_or(Error::ArgumentOutOfBounds)?)
    }

    pub fn write_register<R>(&mut self, reg: R) -> Result<(), Error<E>>
        where R: CurrentAddress + Size + Serialize
    {
        self.wait_for_chip()?;
        // Data size is 1 command byte + 1..4 register bytes
        let mut data = [0; 5];
        let mut comms = COMMS::new();
        comms.set_WEN(false);
        comms.set_RW(false);
        comms.set_RS(reg.current_addr());
        data[0] = comms.bits();

        reg.into_bytes(&mut data[1..]);

        self.spi.transfer(&mut data[..R::size() + 1]).map_err(Error::SPI)?;
        Ok(())
    }

    // todo: check if other errors occur, add timeout counter
    fn wait_for_chip(&mut self) -> Result<(), Error<E>> {
        while self.read_register::<ERROR>()?.SPI_IGNORE_ERR() {
            compiler_fence(Ordering::SeqCst);
        }
        Ok(())
    }

    fn calculate_rtd_temperature_cvd(r: f32, coeff: &CallendarVanDusenCoeff) -> f32 {
        let r0 = coeff.r0;
        let a = coeff.a;
        let b = coeff.b;
        (-a + F32Ext::sqrt(F32Ext::powi(a, 2) - 4f32 * b * (1f32 - r/r0))) / (2f32 * b)
    }
}