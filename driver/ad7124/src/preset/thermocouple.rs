//! AD7124-8 multiple thermocouple sensors according to [CN-0384](https://www.analog.com/media/en/reference-design-documentation/reference-designs/cn0384.pdf).
//!


use crate::register::*;
use crate::{Ad7124, Error, N_BIT};
use embedded_hal::blocking::spi::Transfer;
use core::fmt::Debug;
use crate::preset::CallendarVanDusenCoeff;

pub struct Config<const N: usize> {
    /// Configuration parameters for each RTD sensor.
    pub thermocouple_channels: [Option<ThermocoupleChannel>; N],
    /// Cold junction RTD coefficients.
    pub cold_junction_channel: ColdJunctionChannel,
    /// Internal measurement gain.
    pub gain: Gain,
}

pub struct Circuit<const N: usize> {
    /// Pins for each RTD sensor.
    pub thermocouple_ios: [ThermocoupleIO; N],
    /// Pins for the cold junction RTD sensor.
    pub cold_junction: ColdJunctionIO,
}

pub struct ThermocoupleIO {
    /// Positive sense wire.
    pub sense_p: Input,
    /// Negative sense wire.
    pub sense_n: Input,
}

pub struct ColdJunctionIO {
    /// Positive sense wire.
    pub sense_p: Input,
    /// Negative sense wire.
    pub sense_n: Input,
    /// Sensor current source.
    pub source_p: u8,
    /// Value of the reference resistor connected to REF1.
    pub ref_resistor: u32,
}

pub struct ThermocoupleChannel {
    /// Thermocouple coefficients.
    pub coeff: CallendarVanDusenCoeff,
}

pub struct ColdJunctionChannel {
    /// Excitation current that is applied to IOUT0 and IOUT1.
    pub current: ExcitationCurrent,
    /// Cold junction measurement gain.
    pub gain: Gain,
    /// Thermocouple coefficients.
    pub coeff: CallendarVanDusenCoeff,
}


pub struct Ad7124Thermocouple<Spi, const N: usize> {
    /// Underlying ADC driver. Allows direct access to registers.
    pub adc: Ad7124<Spi>,
    config: Config<{ N }>,
    circuit: Circuit<{ N }>,
    _active_channel: Option<usize>,
}

impl<Spi, E, const N: usize> Ad7124Thermocouple<Spi, { N }>
    where
        Spi: Transfer<u8, Error = E>,
        E: Debug,
{
    const COLD_JUNCTION_CHANNEL: u8 = 7;

    pub fn new(adc: Ad7124<Spi>, config: Config<{ N }>, circuit: Circuit<{ N }>) -> Result<Self, Error<E>> {
        assert!(N <= 6);
        assert!(N > 0);

        let mut adc_thermocouple = Ad7124Thermocouple {
            adc,
            config,
            circuit,
            _active_channel: None,
        };

        adc_thermocouple.configure()?;
        Ok(adc_thermocouple)
    }

    fn configure(&mut self) -> Result<(), Error<E>> {
        // Disable channel 0 which is enabled by default
        // Note(unsafe): Channel is guaranteed to be within range.
        let mut ch = unsafe { CHANNEL::new(0).unwrap_unchecked() };
        ch.set_ENABLE(false);
        ch.set_AINM(Input::AIN1);
        self.adc.write_register(ch)?;

        let mut control = ADC_CONTROL::new();
        control.set_REF_EN(true);
        control.set_POWER_MODE(PowerMode::FullPower);
        self.adc.write_register(control)?;
        //let _control = self.adc.read_register::<ADC_CONTROL>()?;

        // Config cold junction RTD sensor.
        // Note(unsafe): Channel is guaranteed to be within range.
        let mut cfg = unsafe { CONFIG::new(Self::COLD_JUNCTION_CHANNEL).unwrap_unchecked() };
        cfg.set_BIPOLAR(true);
        cfg.set_AIN_BUFP(true);
        cfg.set_AIN_BUFM(true);
        cfg.set_REF_BUFP(true);
        cfg.set_REF_BUFM(true);
        cfg.set_REF_SEL(Ref::REFIN1);
        cfg.set_PGA(self.config.cold_junction_channel.gain);
        self.adc.write_register(cfg)?;
        //let _cfg = self.adc.read_register_offset::<CONFIG>(7)?;

        // Enable excitation current for the cold junction sensor.
        let mut io_ctrl = IO_CONTROL_1::new();
        io_ctrl.set_IOUT0(self.config.cold_junction_channel.current);
        io_ctrl.set_IOUT0_CH(self.circuit.cold_junction.source_p as u32);
        self.adc.write_register(io_ctrl)?;
        //let _io = self.adc.read_register::<IO_CONTROL_1>()?;

        // Note(unsafe): Channel is guaranteed to be within range.
        let mut ch = unsafe { CHANNEL::new(Self::COLD_JUNCTION_CHANNEL).unwrap_unchecked() };
        ch.set_ENABLE(false);
        ch.set_SETUP(Self::COLD_JUNCTION_CHANNEL as u16);
        ch.set_AINP(self.circuit.cold_junction.sense_p);
        ch.set_AINM(self.circuit.cold_junction.sense_n);
        self.adc.write_register(ch)?;
        //let _ch = self.adc.read_register_offset::<CHANNEL>(7)?;


        // Config thermocouple

        // Enable bias on thermocouple prin
        // todo: read from config
        // todo: loop
        let mut io_ctrl = IO_CONTROL_2::new();
        io_ctrl.set_VBIAS0(true);
        io_ctrl.set_VBIAS2(true);
        io_ctrl.set_VBIAS4(true);
        io_ctrl.set_VBIAS6(true);
        self.adc.write_register(io_ctrl)?;
        //let _io_ctrl = self.adc.read_register::<IO_CONTROL_2>()?;

        let mut cfg = unsafe { CONFIG::new(0).unwrap_unchecked() };
        cfg.set_AIN_BUFP(true);
        cfg.set_AIN_BUFM(true);
        cfg.set_REF_BUFP(true);
        cfg.set_REF_BUFM(true);
        cfg.set_REF_SEL(Ref::Internal);
        cfg.set_PGA(self.config.gain);
        self.adc.write_register(cfg)?;
        //let _cfg = self.adc.read_register_offset::<CONFIG>(0)?;

        let mut ch = unsafe { CHANNEL::new(0).unwrap_unchecked() };
        ch.set_ENABLE(true);
        ch.set_SETUP(0);
        ch.set_AINP(self.circuit.thermocouple_ios[0].sense_p);
        ch.set_AINM(self.circuit.thermocouple_ios[0].sense_n);
        self.adc.write_register(ch)?;
        //let _ch = self.adc.read_register_offset::<CHANNEL>(0)?;

        //self.adc.full_internal_calibration(0)?;

        //ch.set_ENABLE(true);
        //self.adc.write_register(ch)?;

        /*
        for i in 0..N {
            // Note(unsafe): Channel index is checked when its written to `self`.
            let mut cfg = unsafe { CONFIG::new(i as u8).unwrap_unchecked() };
            cfg.set_BIPOLAR(true);
            cfg.set_AIN_BUFP(true);
            cfg.set_AIN_BUFM(true);
            cfg.set_REF_BUFP(true);
            cfg.set_REF_BUFM(true);
            cfg.set_REF_SEL(Ref::REFIN1);
            cfg.set_PGA(self.config.gain);
            self.adc.write_register(cfg)?;
        }

         */

        Ok(())
    }

    /*
    pub fn calibrate(&mut self) -> Result<(), Error<E>>  {
        for channel in 0..N {
            // Note(unsafe): Channel index checked in `Self::new()`
            let mut ch = unsafe { CHANNEL::new(channel as u8).unwrap_unchecked() };
            ch.set_ENABLE(false);
            ch.set_SETUP(channel as u16);
            ch.set_AINP(self.circuit.thermocouple_ios[channel].sense_p);
            ch.set_AINM(self.circuit.thermocouple_ios[channel].sense_n);
            self.adc.write_register(ch)?;
        }

        for channel in 0..N {
            self.adc.full_internal_calibration(channel as u8)?;
        }
        Ok(())
    }
    */

/*
    pub fn set_active_channel(&mut self, channel: usize) -> Result<(), Error<E>> {
        if channel >= { N } && self.config.rtd_channels[channel].is_none() {
            return Err(Error::ArgumentOutOfBounds);
        }

        // Disable active channel
        if let Some(active_channel) = self.active_channel.take() {
            // Note(unsafe): Channel index is checked before its written to `self`.
            let mut ch = unsafe { CHANNEL::new(active_channel as u8).unwrap_unchecked() };
            ch.set_ENABLE(false);
            self.adc.write_register(ch)?;
        }

        // Switch current source output
        let mut io_ctl = IO_CONTROL_1::new();
        io_ctl.set_IOUT0(self.config.current);
        io_ctl.set_IOUT1(self.config.current);
        io_ctl.set_IOUT0_CH(self.circuit.thermocouple_ios[channel].source_p as u32);
        io_ctl.set_IOUT1_CH(self.circuit.thermocouple_ios[channel].source_n as u32);
        self.adc.write_register(io_ctl)?;

        // Enable new active channel
        // Note(unsafe): Channel index checked at function entry.
        let mut ch = unsafe { CHANNEL::new(channel as u8).unwrap_unchecked() };
        ch.set_ENABLE(true);
        ch.set_SETUP(channel as u16);
        ch.set_AINP(self.circuit.thermocouple_ios[channel].sense_p);
        ch.set_AINM(self.circuit.thermocouple_ios[channel].sense_n);
        self.adc.write_register(ch)?;

        self.active_channel = Some(channel);
        Ok(())

    }

 */

    /// Reads the measurement of the active channel. Then calculates the resistance
    /// and the RTD temperature using the Callendar-Van Dusen equation.
    ///
    /// **Note:** Supported temperature range is 0°C..500°C, otherwhise no
    /// temperature will be provided.
    pub fn read_cold_junction(&mut self) -> nb::Result<f32, Error<E>> {
        let raw = self.adc.read_measurement()?;

        let gain: u32 = self.config.cold_junction_channel.gain.into();

        // Conversion formula for bipolar measurement according to Analog Devices.
        let resistance = ((raw as i64 - (1 << (N_BIT-1))) * self.circuit.cold_junction.ref_resistor as i64) as f32 / (gain as i32 * (1 << (N_BIT-1))) as f32;

        let temperature = Ad7124::<Spi>::calculate_rtd_temperature_cvd(resistance, &self.config.cold_junction_channel.coeff);

        // Fornula is only valid for positive temperatures and values above
        // 500°C are not sensible.
        if temperature > 0.0 && temperature < 500.0 {
            Ok(temperature)
        } else {
            Err(nb::Error::Other(Error::InvalidMeasurement))
        }
    }


    pub fn read_thermocouple(&mut self) -> nb::Result<f32, Error<E>> {
        let raw = self.adc.read_measurement()?;

        let gain: u32 = self.config.gain.into();

        // Conversion formula for bipolar measurement according to Analog Devices.
        let voltage = ((raw as i64) - (1 << (N_BIT-1))) as f32 * 2.5f32 / ((1 << (N_BIT-1)) * gain) as f32;

        Ok(voltage)
    }
}