//! AD7124-8 multiple 3-wire RTD configuration according to [CN-0383](https://www.analog.com/media/en/reference-design-documentation/reference-designs/cn0383.pdf).
//!


use crate::register::*;
use crate::{Ad7124, Error, N_BIT};
use embedded_hal::blocking::spi::Transfer;
use core::fmt::Debug;
use crate::preset::CallendarVanDusenCoeff;

pub struct Config<const N: usize> {
    /// Configuration parameters for each RTD sensor.
    pub rtd_channels: [Option<RTDChannel>; N],
    /// Excitation current that is applied to IOUT0 and IOUT1.
    pub current: ExcitationCurrent,
    /// Internal measurement gain.
    pub gain: Gain,
}

pub struct Circuit<const N: usize> {
    /// Pins for each RTD sensor.
    pub rtd_ios: [RTD3WIO; N],
    /// Value of the low side reference resistor connected to REF1.
    pub ref_resistor: u32,
}

pub struct RTD3WIO {
    /// Positive sense wire.
    pub sense_p: Input,
    /// Negative sense wire.
    pub sense_n: Input,
    /// Source current for the positive sense wire.
    pub source_p: u8,
    /// Source current for the negative sense wire.
    pub source_n: u8,

}

pub struct RTDChannel {
    /// RTD Coefficient.
    pub coeff: CallendarVanDusenCoeff,
}

pub struct Ad7124Rtd3W<Spi, const N: usize> {
    /// Underlying ADC driver. Allows direct access to registers.
    pub adc: Ad7124<Spi>,
    config: Config<{ N }>,
    circuit: Circuit<{ N }>,
    active_channel: Option<usize>,
}

impl<Spi, E, const N: usize> Ad7124Rtd3W<Spi, { N }>
    where
        Spi: Transfer<u8, Error = E>,
        E: Debug,
{
    pub fn new(adc: Ad7124<Spi>, config: Config<{ N }>, circuit: Circuit<{ N }>) -> Result<Self, Error<E>> {
        assert!(N <= 4);
        assert!(N > 0);

        let mut adc_rtd = Ad7124Rtd3W {
            adc,
            config,
            circuit,
            active_channel: None,
        };

        adc_rtd.configure()?;
        Ok(adc_rtd)
    }

    fn configure(&mut self) -> Result<(), Error<E>> {
        let mut control = ADC_CONTROL::new();
        control.set_REF_EN(true);
        control.set_POWER_MODE(PowerMode::FullPower);
        self.adc.write_register(control)?;

        for i in 0..N {
            // Note(unsafe): Channel index is checked when its written to `self`.
            let mut cfg = unsafe { CONFIG::new(i as u8).unwrap_unchecked() };
            cfg.set_BIPOLAR(true);
            cfg.set_AIN_BUFP(true);
            cfg.set_AIN_BUFM(true);
            cfg.set_REF_BUFP(true);
            cfg.set_REF_BUFM(true);
            cfg.set_REF_SEL(Ref::REFIN1);
            cfg.set_PGA(self.config.gain);
            self.adc.write_register(cfg)?;
        }

        Ok(())
    }

    pub fn calibrate(&mut self) -> Result<(), Error<E>> {
        for channel in 0..N {
            // Note(unsafe): Channel index checked in `Self::new()`
            let mut ch = unsafe { CHANNEL::new(channel as u8).unwrap_unchecked() };
            ch.set_ENABLE(false);
            ch.set_SETUP(channel as u16);
            ch.set_AINP(self.circuit.rtd_ios[channel].sense_p);
            ch.set_AINM(self.circuit.rtd_ios[channel].sense_n);
            self.adc.write_register(ch)?;
        }

        for channel in 0..N {
            self.adc.full_internal_calibration(channel as u8)?;
        }
        Ok(())
    }

    pub fn set_active_channel(&mut self, channel: usize) -> Result<(), Error<E>> {
        if channel >= { N } && self.config.rtd_channels[channel].is_none() {
            return Err(Error::ArgumentOutOfBounds);
        }

        // Disable active channel
        if let Some(active_channel) = self.active_channel.take() {
            // Note(unsafe): Channel index is checked before its written to `self`.
            let mut ch = unsafe { CHANNEL::new(active_channel as u8).unwrap_unchecked() };
            ch.set_ENABLE(false);
            self.adc.write_register(ch)?;
        }

        // Switch current source output
        let mut io_ctl = IO_CONTROL_1::new();
        io_ctl.set_IOUT0(self.config.current);
        io_ctl.set_IOUT1(self.config.current);
        io_ctl.set_IOUT0_CH(self.circuit.rtd_ios[channel].source_p as u32);
        io_ctl.set_IOUT1_CH(self.circuit.rtd_ios[channel].source_n as u32);
        self.adc.write_register(io_ctl)?;

        // Enable new active channel
        // Note(unsafe): Channel index checked at function entry.
        let mut ch = unsafe { CHANNEL::new(channel as u8).unwrap_unchecked() };
        ch.set_ENABLE(true);
        ch.set_SETUP(channel as u16);
        ch.set_AINP(self.circuit.rtd_ios[channel].sense_p);
        ch.set_AINM(self.circuit.rtd_ios[channel].sense_n);
        self.adc.write_register(ch)?;

        self.active_channel = Some(channel);
        Ok(())

    }

    /// Reads the measurement of the active channel. Then calculates the resistance
    /// and the RTD temperature using the Callendar-Van Dusen equation.
    ///
    /// **Note:** Supported temperature range is 0°C..500°C, otherwhise no
    /// temperature will be provided.
    pub fn read_temperature(&mut self) -> nb::Result<f32, Error<E>> {
        let raw = self.adc.read_measurement()?;

        if let Some(channel) = self.active_channel {
            let gain: u32 = self.config.gain.into();
            // Note(unsafe): If the channel was enabled, it checked before it was
            // written to `self`.
            let rtd_channel = unsafe { self.config.rtd_channels[channel].as_ref().unwrap_unchecked() };

            // Conversion formula for bipolar measurement according to Analog
            // Devices. Factor 2 because the wire compensation current which flows
            // through the reference resistor but not through the temperature sensor.
            let resistance = ((raw as i64 - (1 << (N_BIT-1))) * self.circuit.ref_resistor as i64 * 2) as f32 / (gain as i32 * (1 << (N_BIT-1))) as f32;

            let temperature = Ad7124::<Spi>::calculate_rtd_temperature_cvd(resistance, &rtd_channel.coeff);

            // Fornula is only valid for positive temperatures and values above
            // 500°C are not sensible.
            if temperature > 0.0 && temperature < 500.0 {
                Ok(temperature)
            } else {
                Err(nb::Error::Other(Error::InvalidMeasurement))
            }
        } else {
            Err(nb::Error::Other(Error::ArgumentOutOfBounds))
        }
    }
}