pub trait CurrentAddress {
    fn current_addr(&self) -> u8;
}

pub trait Address {
    fn addr() -> u8;
}

pub trait AddressOffset {
    fn addr(offset: u8) -> Option<u8>;
}

pub trait Size {
    /// Size of register in bytes.
    fn size() -> usize;
}

pub trait Deserialize {
    /// Create register from a series of bytes.
    ///
    /// # Safety
    /// - The number of bytes must match the register type.
    /// - `bytes` must be little endian.
    /// - `bytes` must be valid for the register type.
    unsafe fn from_bytes(bytes: &[u8]) -> Self;
}

pub trait DeserializeOffset {
    /// Create register from a series of bytes.
    ///
    /// # Safety
    /// - The number of bytes must match the register type.
    /// - `bytes` must be little endian.
    /// - `bytes` must be valid for the register type.
    unsafe fn from_bytes(bytes: &[u8], offset: u8) -> Option<Self> where Self: Sized;
}

pub trait Serialize {
    /// Convert register into a series of bytes.
    fn into_bytes(self, bytes: &mut [u8]);
}
