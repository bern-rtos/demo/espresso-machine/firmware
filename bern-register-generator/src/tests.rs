extern crate test;
use test::Bencher;
use std::vec::Vec;
use crate::traits::*;
use super::*;

register!{
    REG_XY(0x01|rw): B8 {
        /// Field with documentation
        A: [7..6],
        B: [5..2],
        C: [1],
        D: [0],
    }
}

#[test]
fn addr() {
    assert_eq!(REG_XY::addr(), 0x01);
}

#[test]
fn set_and_get() {
    let mut reg = REG_XY::new();

    reg.set_A(0x3);
    reg.set_B(0x5);
    reg.set_C(false);
    reg.set_D(true);

    assert_eq!(reg.A(), 0x3);
    assert_eq!(reg.B(), 0x5);
    assert_eq!(reg.C(), false);
    assert_eq!(reg.D(), true);

    assert_eq!(reg.bits(), 0xD5);
}

register!{
    REG_RO(0x01|r): B8 {
        A: [7..6],
        B: [5..2],
        C: [1],
        D: [0],
    }
}

#[test]
fn from() {
    let reg = unsafe { REG_RO::from(0x95) };

    assert_eq!(reg.bits(), 0x95);

    assert_eq!(reg.A(), 0x2);
    assert_eq!(reg.B(), 0x5);
    assert_eq!(reg.C(), false);
    assert_eq!(reg.D(), true);
}

register_enum! {
    #[repr(u8)]
    #[derive(Debug, PartialEq)]
    pub enum Reference {
        R3V3 = 0,
        R1V = 1,
        RExt = 2,
    }
}

register!{
    /// Register Comment
    REG_ENUM(0x01|rw): B8 {
        A(Reference): [7..6],
        B: [5..0],
    }
}

#[test]
fn enum_field() {
    let mut reg = REG_ENUM::new();
    reg.set_A(Reference::R3V3);

    assert_eq!(reg.A(), Reference::R3V3);
}

register!{
    REG_B16(0x01|rw): B16 {
        A: [15..6],
        B: [5..0],
    }
}

#[test]
fn reg_b16() {
    let mut reg = REG_B16::new();

    reg.set_A(0x1AF);
    reg.set_B(0x35);

    assert_eq!(reg.bits(), 0x6BF5);

    assert_eq!(reg.A(), 0x1AF);
    assert_eq!(reg.B(), 0x35);
}

register!{
    REG_B24(0x01|rw): B24 {
        A: [23..6],
        B: [5..0],
    }
}

#[test]
fn reg_b24() {
    let mut reg = REG_B24::new();

    reg.set_A(0x2A1AF);
    reg.set_B(0x35);

    assert_eq!(reg.bits(), 0xA86BF5);

    assert_eq!(reg.A(), 0x2A1AF);
    assert_eq!(reg.B(), 0x35);
}

register!{
    REG_B32(0x01|rw): B32 {
        A: [31..6],
        B: [5..0],
    }
}

#[test]
fn reg_b32() {
    let mut reg = REG_B32::new();

    reg.set_A(0x2C2A1AF);
    reg.set_B(0x35);

    assert_eq!(reg.bits(), 0xB0A86BF5);

    assert_eq!(reg.A(), 0x2C2A1AF);
    assert_eq!(reg.B(), 0x35);
}

register!{
    REG_B32_ENUM(0x01|rw): B32 {
        A(Reference): [31..30],
        B: [29..0],
    }
}

#[test]
fn enum_field_b32() {
    let mut reg = REG_B32_ENUM::new();

    reg.set_A(Reference::RExt);
    reg.set_B(0x35);

    assert_eq!(reg.A(), Reference::RExt);
    assert_eq!(reg.B(), 0x35);
}

register!{
    [REG; 4](0x01 | rw): B16 {
        A: [15..12],
        B: [11..0],
    }
}

#[test]
fn reapeating_register() {
    let mut reg = REG::new(0).unwrap();

    reg.set_A(0x5);
    reg.set_B(0x35);

    assert_eq!(reg.A(), 0x5);
    assert_eq!(reg.B(), 0x35);

    assert_eq!(REG::new(4).is_none(), true);
}

register!{
    REG_1B(0x01 | rw): B8 {
        A: [7..0],
    }
}
register!{
    REG_2B(0x01 | rw): B16 {
        B: [15..8],
        A: [7..0],
    }
}
register!{
    REG_3B(0x01 | rw): B24 {
        C: [23..16],
        B: [15..8],
        A: [7..0],
    }
}
register!{
    REG_4B(0x01 | rw): B32 {
        D: [31..24],
        C: [23..16],
        B: [15..8],
        A: [7..0],
    }
}

#[test]
fn serialize() {
    let mut data = [0u8; 4];
    let mut reg = REG_1B::new();
    reg.set_A(0xAA);
    reg.into_bytes(&mut data[..1]);
    assert_eq!(data, [0xAA, 0x00, 0x00, 0x00]);

    let mut data = [0u8; 4];
    let mut reg = REG_2B::new();
    reg.set_A(0xAA);
    reg.set_B(0xBB);
    reg.into_bytes(&mut data[..2]);
    assert_eq!(data, [0xBB, 0xAA, 0x00, 0x00]);

    let mut data = [0u8; 4];
    let mut reg = REG_3B::new();
    reg.set_A(0xAA);
    reg.set_B(0xBB);
    reg.set_C(0xCC);
    reg.into_bytes(&mut data[..3]);
    assert_eq!(data, [0xCC, 0xBB, 0xAA, 0x00]);

    let mut data = [0u8; 4];
    let mut reg = REG_4B::new();
    reg.set_A(0xAA);
    reg.set_B(0xBB);
    reg.set_C(0xCC);
    reg.set_D(0xDD);
    reg.into_bytes(&mut data);
    assert_eq!(data, [0xDD, 0xCC, 0xBB, 0xAA]);
}

#[test]
fn deserialize() {
    let data = [0xDDu8, 0xCCu8, 0xBBu8, 0xAAu8];

    let reg = unsafe { REG_1B::from_bytes(&data[3..]) };
    assert_eq!(reg.A(), 0xAA);

    let reg = unsafe { REG_2B::from_bytes(&data[2..]) };
    assert_eq!(reg.A(), 0xAA);
    assert_eq!(reg.B(), 0xBB);

    let reg = unsafe { REG_3B::from_bytes(&data[1..]) };
    assert_eq!(reg.A(), 0xAA);
    assert_eq!(reg.B(), 0xBB);
    assert_eq!(reg.C(), 0xCC);

    let reg = unsafe { REG_4B::from_bytes(&data) };
    assert_eq!(reg.A(), 0xAA);
    assert_eq!(reg.B(), 0xBB);
    assert_eq!(reg.C(), 0xCC);
    assert_eq!(reg.D(), 0xDD);
}


register!{
     REG_BENCH(0x01|rw): B32 {
        UPPER: [31..17],
        BIT: [16],
        LOWER: [14..0],
    }
}
#[bench]
fn bench_access_upper(bencher: &mut Bencher) {
    let mut reg = REG_BENCH::new();

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg.set_UPPER(v);
            //assert_eq!(reg.UPPER(), v);
            reg.UPPER()
        }).collect::<Vec<u32>>()
    })
}
#[bench]
fn bench_access_lower(bencher: &mut Bencher) {
    let mut reg = REG_BENCH::new();

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg.set_LOWER(v);
            //assert_eq!(reg.LOWER(), v);
            reg.LOWER()
        }).collect::<Vec<u32>>()
    })
}
#[bench]
fn bench_access_bit(bencher: &mut Bencher) {
    let mut reg = REG_BENCH::new();

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg.set_BIT(v % 2 == 0);
            //assert_eq!(reg.BIT(), v % 2 == 0);
            reg.BIT()
        }).collect::<Vec<bool>>()
    })
}

#[bench]
fn bench_access_upper_manual(bencher: &mut Bencher) {
    let mut reg = 0u32;

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg = (reg & !0xFFFE0000) | (v << 17);
            //assert_eq!((reg & 0xFFFE0000) >> 17, v);
            (reg & 0xFFFE0000) >> 17
        }).collect::<Vec<u32>>()
    })
}
#[bench]
fn bench_access_lower_manual(bencher: &mut Bencher) {
    let mut reg = 0u32;

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg = (reg & !0x7FFF) | v;
            //assert_eq!(reg & 0x7FFF, v);
            reg & 0x7FFF
        }).collect::<Vec<u32>>()
    })
}
#[bench]
fn bench_access_bit_manual(bencher: &mut Bencher) {
    let mut reg = 0u32;

    bencher.iter(|| {
        (0..0x7FFE).map(|v| {
            reg = (reg & !0x10000) | (((v % 2 == 0) as u32) << 16);
            //assert_eq!(reg & 0x10000 > 0, v % 2 == 0);
            reg & 0x10000 > 0
        }).collect::<Vec<bool>>()
    })
}
