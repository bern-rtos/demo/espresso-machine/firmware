//! # Register Generator
//!
//! Device drivers need register defintions which cannot be generated from SVD
//! files. This crate aim to provide a similar interface as peripheral access
//! crates with a intuitive interface.
//!
//! ## Examples
//!
//! Consider an accelerometer which provides a value and a few flags in a
//! 16bit register at address `0xA0`
//!
//! ```
//! use bern_register_generator::register;
//! register!{
//!     ACCEL(0xA0|r): B16 {
//!         DATA_VALID: [15],
//!         ALARM: [14],
//!         VALUE_X: [13..7],
//!         VALUE_Y: [6..0],
//!     }
//! }
//!
//! fn main() {
//!     // Read value from SPI/I2C interface
//!     let mut accel = unsafe { ACCEL::from(0xB105) };
//!     if accel.DATA_VALID() {
//!         println!("x: {}, y: {}",
//!             accel.VALUE_X(),
//!             accel.VALUE_Y(),
//!         );
//!     }
//! }
//! ```
//!
//! ## Limitations
//!
//! - Bitfields are not checked for overlap
//! - Bitfield address order is not checked
//! - Bitfield size is checked against underlying type
//!

#![cfg_attr(target_os = "none", no_std)]
#![cfg_attr(not(target_os = "none"), feature(test))]

pub mod register_types;
pub mod traits;

#[cfg(test)]
mod tests;

#[allow(unused_imports)]
pub use concat_idents::concat_idents;

#[doc(hidden)]
#[macro_export]
macro_rules! _check_access {
    (r) => {};
    (rw) => {};
    ($x:expr) => { compile_error!("Register access can either be `r` for read-only or `rw` for read-write."); };
}

#[macro_export(local_inner_macros)]
macro_rules! register {
    (
        $(#[$doc:meta])*
        $register:ident($addr:literal | $access:tt): $reg_ty:ident $fields:tt
    ) => {
        _check_access!{$access}
        _register_impl!{($($doc),*), $register, (), $addr, $reg_ty, $access, $fields}
    };
    (
        $(#[$doc:meta])*
        [$register:ident; $n_reg:literal] ($addr:literal | $access:tt): $reg_ty:ident $fields:tt
    ) => {
        _check_access!{$access}
        _register_impl!{($($doc),*), $register, ($n_reg), $addr, $reg_ty, $access, $fields}
    };
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! _if_rw {
    (r, $value:tt) => {};
    (rw, $value:tt) => {$value};
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! _register_impl {
    (
        ( $($reg_doc:meta),* ),
        $register:ident, ($($n_reg:literal)?), $addr:literal, $reg_ty:ident, $access:tt,
        {$(
                $(#[$doc:meta])*
                $field:ident$(($field_ty:ident))*: $bits:tt,
        )+}
    ) => {
        $(#[$reg_doc])*
        #[allow(non_camel_case_types)]
        #[derive(Copy, Clone)]
        pub struct $register {
            bits: $crate::register_types::$reg_ty::$reg_ty,
            addr: u8,
        }

        impl $register {
            const BASE_ADDR: u8 = $addr;

            _register_impl_new!{$access, $register, $addr, $reg_ty $(, $n_reg)?}

            _register_from_impl!{$register, $addr, $reg_ty $(, $n_reg)?}

            #[allow(unused)]
            #[inline(always)]
            pub fn bits(&self) -> $crate::register_types::$reg_ty::Repr  {
                self.bits.raw()
            }

            $(
                _register_field!{$access, ($register, $reg_ty, $field $(, $doc)*), $bits $(, $field_ty)* }
            )*
        }

        _register_addr_impl!{$register $(, $n_reg)?}

        impl $crate::traits::CurrentAddress for $register {
            fn current_addr(&self) -> u8 {
                self.addr
            }
        }

        impl $crate::traits::Size for $register {
            fn size() -> usize {
                 $crate::register_types::$reg_ty::BYTES
            }
        }

        _register_deserialize_impl!{$register, $reg_ty $(, $n_reg)?}

        impl $crate::traits::Serialize for $register {
            fn into_bytes(self, bytes: &mut [u8]) {
                for i in 0..$crate::register_types::$reg_ty::BYTES {
                    bytes[i] = ((self.bits.raw() >> (8 * ($crate::register_types::$reg_ty::BYTES - i - 1))) & 0xFF) as u8;
                }
            }
        }
    }
}

#[doc(hidden)]
#[macro_export]
macro_rules! _register_impl_new {
    // Single read-write register
    (rw, $register:ident, $addr:literal, $reg_ty:ident) => {
        #[allow(unused)]
        pub const fn new() -> $register {
            $register {
                bits: $crate::register_types::$reg_ty::$reg_ty::new(0),
                addr: $addr,
            }
        }
    };
    // Multiple read-write register with different addresses
    (rw, $register:ident, $addr:literal, $reg_ty:ident, $n_regs:literal) => {
        #[allow(unused)]
        pub const fn new(offset: u8) -> Option<$register> where Self: Sized {
            if offset >= $n_regs {
                return None;
            }

            // Note(unsafe): We just checked whether offset is in range.
            Some($register {
                bits: $crate::register_types::$reg_ty::$reg_ty::new(0),
                addr: $addr + offset,
            })
        }
    };
    // read-only registers can only be create using `::from()`
    (r, $register:ident, $addr:literal, $reg_ty:ident) => { }
}

#[doc(hidden)]
#[macro_export]
macro_rules! _register_from_impl {
    ($register:ident, $addr:literal, $reg_ty:ident) => {
        /// Create a register from bits.
        ///
        /// # Safety
        /// It is your responsibility to feed the right bits to right register.
        #[allow(unused)]
        pub const unsafe fn from(bits: $crate::register_types::$reg_ty::Repr) -> Self {
            $register {
                bits: $crate::register_types::$reg_ty::$reg_ty::new(bits),
                addr: $addr,
            }
        }
    };
    ($register:ident, $addr:literal, $reg_ty:ident, $n_regs:literal) => {
        /// Create a register from bits.
        ///
        /// # Safety
        /// It is your responsibility to feed the right bits to right register.
        #[allow(unused)]
        pub const unsafe fn from(bits: $crate::register_types::$reg_ty::Repr, offset: u8) -> Option<Self> {
            if offset >= $n_regs {
                return None;
            }

            Some($register {
                bits: $crate::register_types::$reg_ty::$reg_ty::new(bits),
                addr: $addr + offset,
            })
        }
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! _register_deserialize_impl {
    ($register:ident, $reg_ty:ident) => {
        impl $crate::traits::Deserialize for $register {
            unsafe fn from_bytes(bytes: &[u8]) -> Self {
                let mut bits: $crate::register_types::$reg_ty::Repr = 0;
                for (i, b) in bytes.iter().enumerate() {
                    bits |= (*b as $crate::register_types::$reg_ty::Repr) << (8 * ($crate::register_types::$reg_ty::BYTES - i - 1));
                }
                Self::from(bits)
            }
        }
    };
    ($register:ident, $reg_ty:ident, $n_regs:literal) => {
        impl $crate::traits::DeserializeOffset for $register {
            unsafe fn from_bytes(bytes: &[u8], offset: u8) -> Option<Self> where Self: Sized {
                let mut bits: $crate::register_types::$reg_ty::Repr = 0;
                for (i, b) in bytes.iter().enumerate() {
                    bits |= (*b as $crate::register_types::$reg_ty::Repr) << (8 * ($crate::register_types::$reg_ty::BYTES - i - 1));
                }
                Self::from(bits, offset)
            }
        }
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! _register_addr_impl {
    ($register:ident) => {
        impl $crate::traits::Address for $register {
            fn addr() -> u8 {
                Self::BASE_ADDR
            }
        }
    };
    ($register:ident, $n_regs:literal) => {
        impl $crate::traits::AddressOffset for $register {
            fn addr(offset: u8) -> Option<u8> {
                if offset >= $n_regs {
                    None
                } else {
                    Some(Self::BASE_ADDR + offset)
                }
            }
        }
    };
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! _register_field {
    (rw, $reg_args:tt, [$lsb:literal]) => {
        _register_field_impl!{$reg_args, $lsb, $lsb, bool}
        _register_field_impl_mut!{$reg_args, $lsb, $lsb, bool}
    };
    (rw, ($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), [$msb:literal..$lsb:literal]) => {
        _register_field_impl!{($register, $reg_ty, $field $(, $doc)*), $msb, $lsb, $crate::register_types::$reg_ty::Repr}
        _register_field_impl_mut!{($register, $reg_ty, $field $(, $doc)*), $msb, $lsb, $crate::register_types::$reg_ty::Repr}
    };
    (rw, $reg_args:tt, [$msb:literal..$lsb:literal], $field_ty:ident) => {
        _register_field_impl!{$reg_args, $msb, $lsb, $field_ty}
        _register_field_impl_mut!{$reg_args, $msb, $lsb, $field_ty}
    };

    (r, $reg_args:tt, [$lsb:literal]) => {
        _register_field_impl!{$reg_args, $lsb, $lsb, bool}
    };
    (r, ($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), [$msb:literal..$lsb:literal]) => {
        _register_field_impl!{($register, $reg_ty, $field $(, $doc)*), $msb, $lsb, $crate::register_types::$reg_ty::Repr}
    };
    (r, $reg_args:tt, [$msb:literal..$lsb:literal], $field_ty:ident) => {
        _register_field_impl!{$reg_args, $msb, $lsb, $field_ty}
    };
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! _register_field_impl_mut {
    // Implement for boolean specifically to increase access speed.
    (($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), $msb:literal, $lsb:literal, bool) => {
        concat_idents!(fn_name = set_, $field {
            $(#[$doc])*
            #[allow(unused)]
            #[allow(non_snake_case)]
            #[inline(always)]
            pub fn fn_name(&mut self, value: bool) {
                self.bits = $crate::register_types::$reg_ty::$reg_ty::new(self.bits.raw() & !(1 << $lsb) | ((value as $crate::register_types::$reg_ty::Repr) << $lsb));
            }
        });
    };
    (($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), $msb:literal, $lsb:literal, $ty:ty) => {
        concat_idents!(fn_name = set_, $field {
            $(#[$doc])*
            #[allow(unused)]
            #[allow(non_snake_case)]
            #[inline(always)]
            pub fn fn_name(&mut self, value: $ty) {
                self.bits = (self.bits & !$crate::register_types::$reg_ty::$reg_ty::mask($msb, $lsb)) | ((Into::<$crate::register_types::$reg_ty::$reg_ty>::into(value) << $lsb) & $crate::register_types::$reg_ty::$reg_ty::mask($msb, $lsb));
            }
        });
    };
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! _register_field_impl {
    (($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), $msb:literal, $lsb:literal, bool) => {
        $(#[$doc])*
        #[allow(unused)]
        #[allow(non_snake_case)]
        #[inline(always)]
        pub fn $field(&self) -> bool {
            (self.bits.raw() & (1 << $lsb)) > 0
        }
    };
    (($register:ident, $reg_ty:ident, $field:ident $(, $doc:meta)*), $msb:literal, $lsb:literal, $ty:ty) => {
        $(#[$doc])*
        #[allow(unused)]
        #[allow(non_snake_case)]
        #[inline(always)]
        pub fn $field(&self) -> $ty {
            Into::<$ty>::into((self.bits & $crate::register_types::$reg_ty::$reg_ty::mask($msb, $lsb)) >> $lsb)
        }
    };
}



#[doc(hidden)]
#[macro_export]
macro_rules! _enum_from_register {
    ($field_ty:ident, $reg_ty:ident) => {
        impl From<$field_ty> for $crate::register_types::$reg_ty::$reg_ty {
            fn from(value: $field_ty) -> Self {
                $crate::register_types::$reg_ty::$reg_ty::new(value as $crate::register_types::$reg_ty::Repr)
            }
        }
        impl From<$crate::register_types::$reg_ty::$reg_ty> for $field_ty {
            fn from(value: $crate::register_types::$reg_ty::$reg_ty) -> Self {
                unsafe { core::mem::transmute((value.raw() & 0xFF) as u8) }
            }
        }
    }
}

#[macro_export(local_inner_macros)]
macro_rules! register_enum {
    (
        #[repr(u8)]
        $(#[$attr:meta])*
        $vis:vis enum $enum:ident $values:tt
    ) => {
        #[repr(u8)]
        $(#[$attr])*
        $vis enum $enum $values

        _enum_from_register!{$enum, B8}
        _enum_from_register!{$enum, B16}
        _enum_from_register!{$enum, B24}
        _enum_from_register!{$enum, B32}
    }
}
